package com.jira.backlog.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.left;

@Getter
public enum TaskStatus {
    SPECIFICATION("Specification", "#d82222", "SPE", 3),
    ESTIMATION("Estimation", "#d82222", "EST", 2),
    READY_TO_DEVELOP("Ready to Develop", "#d82222", "RTD", 4),
    DEVELOPING("Developing", "#FFAA00", "DEV", 5),
    REVIEW("Review", "#1579af", "REV", 3),
    CORRECTING("Correcting", "#d82222", "COR", 1),
    READY_FOR_TESTING("Ready for Testing", "#16af2b", "RFT", 6),
    TESTING("Testing", "#16af2b", "TST", 7),
    READY("Ready", "#16af2b", "RDY", 8),
    READY_TO_RELEASE("Ready to Release", "#16af2b", "RTR", 9),
    RELEASED("Released", "#16af2b", "REL", 10),
    DONE("Done", "#16af2b", "DONE", 11);

    private final Status status;

    TaskStatus(String status, String fontColor, String shortCode, Integer priority) {
        this.status = new Status(status, fontColor, shortCode, priority);
    }

    private static final Map<String, Status> TaskStatusMap = Stream.of(TaskStatus.values())
            .collect(Collectors.toMap(toLower(t -> t.getStatus().getStatus()), TaskStatus::getStatus));

    public static Status findStatus(String status) {
        return TaskStatusMap.getOrDefault(status.toLowerCase(), new Status(status, null, left(status.toUpperCase(), 3), Integer.MAX_VALUE));
    }

    private static <T> Function<T, String> toLower(Function<T, String> mapper) {
        return mapper.andThen(String::toLowerCase);
    }

    @Getter
    @AllArgsConstructor
    public static class Status {
        private final String status;
        private final String fontColor;
        private final String shortCode;
        private final Integer priority;
    }
}
