package com.jira.backlog.model;

import com.atlassian.jira.rest.client.api.domain.AddressableNamedEntity;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.IssueField;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONObject;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

@Data
public class Task {
    private static final Pattern TASK_INFORMATION_PATTERN = Pattern.compile("^\\s*([a-z]+-\\d+)\\s*(\\d*)\\s*(.*)\\s*$", Pattern.CASE_INSENSITIVE);
    private Long id;
    private String ticket;
    private String summary;
    private TaskStatus.Status status;
    private String statusValue;
    private String assignee;
    private Integer priority;
    private String comment;
    private Integer idx;

    public Task(String taskInformation) {
        Matcher taskMatcher = TASK_INFORMATION_PATTERN.matcher(taskInformation);
        if (taskMatcher.matches()) {
            this.ticket = taskMatcher.group(1);
            String priority = taskMatcher.group(2);
            if (StringUtils.isNotEmpty(priority)) {
                this.priority = Integer.valueOf(priority);
            }
            this.comment = taskMatcher.group(3);
        }
    }

    public void updateFields(Issue issue) {
        try {
            this.assignee = Optional.ofNullable(issue.getAssignee()).map(AddressableNamedEntity::getName).orElse(null);
            this.summary = issue.getSummary();
            this.statusValue = issue.getStatus().getName();
            this.status = TaskStatus.findStatus(this.statusValue);
            this.ticket = issue.getKey();
            this.id = issue.getId();
        } catch (Exception e) {

        }
    }


}
