package com.jira.backlog;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.VerticalFlowLayout;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import com.jira.backlog.model.Task;
import com.jira.backlog.model.TaskStatus;
import com.jira.backlog.service.JiraService;
import com.jira.backlog.service.StoreService;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.HorizontalLayout;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("all")
public class JiraBacklogWindow implements ToolWindowFactory {

    private static String SAVE_BUTTON_TEXT;

    static {
        try {
            SAVE_BUTTON_TEXT = new String("Сохранить".getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            SAVE_BUTTON_TEXT = "Save";
        }
    }

    private Vector<String> columnsHeader = new Vector<>(Arrays.asList("Task", "Status", "Assignee"));

    private JTextField jiraUrlField;
    private JTextField ticketField;
    private JTextField loginField;
    private JPasswordField passwordField;
    private StoreService storeService;
    private JiraService jiraService;
    private ToolWindow toolWindow;
    private boolean showSettings;

    public void createToolWindowContent(Project project, ToolWindow toolWindow) {
        this.storeService = StoreService.getInstance();
        this.jiraService = JiraService.getInstance();
        this.toolWindow = toolWindow;
        if (checkJira()) {
            this.showSettings = false;
        }
        drawContent();
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(() -> drawContent(), 1, 30, TimeUnit.SECONDS);
    }

    private void drawContent() {
        Content content;
        if (showSettings) {
            content = drawSettingsScreen();
        } else {
            content = drawTasksScreen();
        }
        toolWindow.getContentManager().removeAllContents(true);
        toolWindow.getContentManager().addContent(content);
    }

    @NotNull
    private Content drawSettingsScreen() {
        JPanel panel = new JPanel();
        panel.setLayout(new VerticalFlowLayout());

        JLabel jiraUrlLabel = new JLabel("JIRA base url");
        jiraUrlField = new JTextField(storeService.getJiraBaseUrl());
        panel.add(jiraUrlLabel);
        panel.add(jiraUrlField);

        JLabel ticketLabel = new JLabel("Ticket");
        ticketField = new JTextField(storeService.getTicket());
        panel.add(ticketLabel);
        panel.add(ticketField);

        JLabel loginLabel = new JLabel("Login");
        loginField = new JTextField(storeService.getLogin());
        panel.add(loginLabel);
        panel.add(loginField);

        JLabel passwordLabel = new JLabel("Password");
        passwordField = new JPasswordField(storeService.getPassword());
        panel.add(passwordLabel);
        panel.add(passwordField);
        JButton submitButton = new JButton(SAVE_BUTTON_TEXT);
        panel.add(submitButton);
        submitButton.addActionListener(action -> onSaveAction(action));

        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        return contentFactory.createContent(panel, "", false);
    }

    @NotNull
    private Content drawTasksScreen() {
        JPanel panel = new JPanel();
        panel.setLayout(new VerticalFlowLayout());

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new HorizontalLayout());
        panel.add(buttonPanel);

        JButton update = new JButton("Update");
        buttonPanel.add(update);
        update.addActionListener(action -> drawContent());

        JButton settings = new JButton("Settings");
        buttonPanel.add(settings);
        settings.addActionListener(action -> {
            this.showSettings = true;
            drawContent();
        });

        List<Task> tickets = jiraService.getCurrentTickets();
        JPanel taskPanel = new JPanel();
        new SpringLayout();
        taskPanel.setLayout(new SpringLayout());
        taskPanel.add(createElement("#", "Priority", null, null, 0b1111, Color.BLACK));
        taskPanel.add(createElement("Key", "Task Key", null, null, 0b1011, Color.BLACK));
        taskPanel.add(createElement("S", "Task Status", null, null, 0b1011, Color.BLACK));
        taskPanel.add(createElement("Assign", null, null, null, 0b1011, Color.BLACK));
        taskPanel.add(createElement("Comment", null, null, null, 0b1011, Color.BLACK));
        tickets.forEach(t -> addTaskToTable(taskPanel, t));
        panel.add(taskPanel);
        SpringUtilities.makeCompactGrid(taskPanel,
                tickets.size() + 1, 5, //rows, cols
                0, 0, 0, 0);
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        return contentFactory.createContent(panel, "", false);
    }

    private void addTaskToTable(JPanel taskPanel, Task t) {
        Integer priorityValue = t.getPriority();
        JLabel priority = createElement((priorityValue == null) ? null : priorityValue.toString(), null, "#FF0000", true, 0b0111, Color.BLACK);
        taskPanel.add(priority);
        JLabel goToTicket = createElement(t.getTicket(), t.getSummary(), "#FFAA00", true, 0b0011, Color.BLACK);
        goToTicket.setCursor(new Cursor(Cursor.HAND_CURSOR));
        goToTicket.addMouseListener(new OpenTicketMouseClickEventListener(t));
        taskPanel.add(goToTicket);
        TaskStatus.Status taskStatus = t.getStatus();
        JLabel status = createElement(taskStatus.getShortCode(), t.getStatusValue(), taskStatus.getFontColor(), true, 0b0011, Color.BLACK);
        taskPanel.add(status);
        JLabel assignee = createElement(t.getAssignee(), null, null, null, 0b0011, Color.BLACK);
        taskPanel.add(assignee);
        JLabel comment = createElement(t.getComment(), null, null, false, 0b0011, Color.BLACK);
        taskPanel.add(comment);
    }

    private JLabel createElement(String text, String tooltipText,
                                 String fontColor, Boolean bold,
                                 int borders, Color borderColor) {
        JLabel label = new JLabel(text);
        if (StringUtils.isNoneEmpty(fontColor) || bold != null) {
            StringBuilder textBuilder = new StringBuilder();
            textBuilder.append("<html>");
            if (StringUtils.isNoneEmpty(fontColor)) {
                textBuilder.append("<font color=\"")
                        .append(fontColor)
                        .append("\">");
            }
            if (bold) {
                textBuilder.append("<b>");
            }
            textBuilder.append(text);
            if (bold) {
                textBuilder.append("</b>");
            }

            if (StringUtils.isNoneEmpty(fontColor)) {
                textBuilder.append("</font>");
            }
            textBuilder.append("</html>");
            label.setText(textBuilder.toString());
        }
        label.setToolTipText(tooltipText);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setBorder(BorderFactory.createMatteBorder(borders >> 3 & 0b1, borders >> 2 & 0b1, borders >> 1 & 0b1, borders & 0b1, borderColor));
        return label;
    }

    private void onSaveAction(ActionEvent action) {
        storeService.setJiraBaseUrl(jiraUrlField.getText());
        storeService.setTicket(ticketField.getText());
        storeService.setLogin(loginField.getText());
        storeService.setPassword(passwordField.getText());
        if (checkJira()) {
            showSettings = false;
            drawContent();
        }
    }

    private boolean checkJira() {
        JiraRestClient jiraRestClient = jiraService.getJiraRestClient();//checking config
        if (jiraRestClient != null) {
            return true;
        }
        return false;
    }

    private class OpenTicketMouseClickEventListener extends MouseAdapter {
        private final Task task;

        public OpenTicketMouseClickEventListener(Task task) {
            this.task = task;
        }

        @Override
        public void mouseClicked(MouseEvent event) {
            if (Desktop.isDesktopSupported()) {
                try {
                    Desktop.getDesktop().browse(new URI(storeService.getJiraBaseUrl() + "/browse/" + task.getTicket()));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
