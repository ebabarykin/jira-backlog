package com.jira.backlog.service;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

@Data
@State(
        name = "JiraBacklogPluginSettings",
        storages = @Storage("JiraBacklogPluginSettings.xml")
)
public class StoreService implements PersistentStateComponent<StoreService> {

    private String jiraBaseUrl;
    private String ticket;
    private String login;
    private String password;

    private StoreService() {
    }

    @Override
    public StoreService getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull StoreService state) {
        XmlSerializerUtil.copyBean(state, this);
    }

    public static StoreService getInstance() {
        return ServiceManager.getService(StoreService.class);
    }
}