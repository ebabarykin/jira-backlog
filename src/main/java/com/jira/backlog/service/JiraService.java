package com.jira.backlog.service;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.intellij.openapi.components.ServiceManager;
import com.jira.backlog.model.Task;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsLast;

@Slf4j
public class JiraService {
    private static final Comparator<Task> taskComparator =
            Comparator.comparing(Task::getPriority, nullsLast(naturalOrder()))
                    .thenComparing(t -> t.getStatus().getPriority(), nullsLast(naturalOrder()))
                    .thenComparingInt(Task::getIdx);


    public static JiraService getInstance() {
        return ServiceManager.getService(JiraService.class);
    }

    public JiraRestClient getJiraRestClient() {
        StoreService storeService = StoreService.getInstance();
        try {
            if (storeService.getJiraBaseUrl() == null) {
                return null;
            }
            URI uri = URI.create(storeService.getJiraBaseUrl());
            JiraRestClient jiraRestClient = new AsynchronousJiraRestClientFactory()
                    .createWithBasicHttpAuthentication(uri, storeService.getLogin(), storeService.getPassword());
            jiraRestClient.getIssueClient().getIssue(storeService.getTicket()).claim();
            return jiraRestClient;
        } catch (Exception e) {
            log.error("error while creating jira client", e);
        }
        return null;
    }

    public List<Task> getCurrentTickets() {
        StoreService storeService = StoreService.getInstance();
        JiraRestClient jiraRestClient = getJiraRestClient();
        if (jiraRestClient == null) {
            return Collections.emptyList();
        }
        IssueRestClient issueClient = jiraRestClient.getIssueClient();
        Issue issue = issueClient.getIssue(storeService.getTicket()).claim();
        if (issue == null || StringUtils.isEmpty(issue.getDescription())) {
            return Collections.emptyList();
        }
        AtomicInteger idx = new AtomicInteger(0);
        List<Task> tasks = Stream.of(issue.getDescription().split("\r?\n+"))
                .filter(StringUtils::isNotEmpty)
                .map(Task::new)
                .peek(t -> t.setIdx(idx.incrementAndGet()))
                .collect(Collectors.toList());
        return tasks.parallelStream()
                .peek(task -> updateTaskInfo(task, issueClient))
                .sorted(taskComparator)
                .collect(Collectors.toList());
    }

    private void updateTaskInfo(Task task, IssueRestClient issueClient) {
        Issue issue = issueClient.getIssue(task.getTicket()).claim();
        task.updateFields(issue);
    }
}
